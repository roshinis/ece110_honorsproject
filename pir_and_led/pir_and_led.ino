// This is code that we were initially using to test the PIR  
// note: all project components are detailed in the final prototype code 
 
void setup() {
  // put your setup code here, to run once:  
  // this code is from sparkfun's pir sensor setup code 
  Serial.begin(9600);
  // The PIR sensor's output signal is an open-collector, 
  // so a pull-up resistor is required:
  pinMode(2, INPUT_PULLUP);
  pinMode(11, OUTPUT); 
  digitalWrite(11, LOW); 

}

void loop() {
  // put your main code here, to run repeatedly: 
  int motion_detected = digitalRead(2); //reads the value from pin 2 which has an internal pull-up resistor    
  // if digitalRead of pin 2 returns low, motion is detected
  if(motion_detected ==  LOW){ 
    digitalWrite(11,HIGH); 
    Serial.println("Led should be on");     
  }
  else { 
    digitalWrite(11,LOW);    
  }
}
