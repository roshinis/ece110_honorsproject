// This is code that we were initially using to test the PIR in conjunction with the QRD1114   
// note: all project components are detailed in final prototype code  

const int OPTICAL_DETECTOR = A0; 
const int LED_PIN = 11;    
const int MOTION_DETECTOR = 2; 

void setup() { 
  // put your setup code here, to run once: 
  Serial.begin(9600);
  pinMode(OPTICAL_DETECTOR,INPUT); 
  pinMode(LED_PIN,OUTPUT);  
  pinMode(MOTION_DETECTOR,INPUT_PULLUP); 
  digitalWrite(LED_PIN,LOW); 
}

void loop() {      
  //turn the led off
  digitalWrite(LED_PIN,LOW);   
   
  // read an analog voltage from the optical detector and turn it into a voltage from 0-5V
  int proximity = analogRead(OPTICAL_DETECTOR); 
  float voltage = ((float)proximity*5.0)/1023;     

  // if the door opening is detected, turn the light on
  if(voltage < 2.5){  
    Serial.println("Lights on!"); 
    digitalWrite(LED_PIN,HIGH); 
    // keep it on for 5 seconds and then scan for motion  
    delay(5000); 
    
    // while motion is detected, keep it on for another 10 seconds and scan again     
    Serial.println("Scanning for motion...");   
    int is_motion = digitalRead(MOTION_DETECTOR);
    while(is_motion == LOW){    
      Serial.println("Motion detected! Keep lights on!");  
      // keep light on for 10 seconds 
      delay(10000);     
      Serial.println("Scanning for motion...");  
      delay(3000);  
      is_motion = digitalRead(MOTION_DETECTOR);  
    }  
    // if motion is not detected, turn the lights off 
    Serial.println("No motion detected, turning off...");
  } 
}
